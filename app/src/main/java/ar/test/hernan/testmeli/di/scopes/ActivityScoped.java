package ar.test.hernan.testmeli.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * En Dagger, un componente sin scope no puede depender de un componente con scope. Como
 * {@link ar.test.hernan.testmeli.di.AppComponent} es un componente de {@link ApplicationScoped}, creamos un scope
 * personalizado.
 * Scope a ser utilizado por todos los componentes de un fragment.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityScoped {
}
