package ar.test.hernan.testmeli.data.source.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ar.test.hernan.testmeli.data.models.Item;

/**
 * Room data base que contiene la tabla de {@link Item}
 * */
@Database(entities = {Item.class}, version = 1, exportSchema = false)
public abstract class ItemsDataBase extends RoomDatabase {
    public abstract ItemsDao itemDao();
}
