package ar.test.hernan.testmeli.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Scope de reemplazo de {@link javax.inject.Singleton}, para mejorar la legibilidad.
 * */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationScoped {
}
