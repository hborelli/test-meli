package ar.test.hernan.testmeli.util.executorutils;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

import ar.test.hernan.testmeli.di.scopes.ApplicationScoped;

/**
 * Ejecutor global para toda la aplicacion.
 * Agrupar tareas para evitar starvation.
 *
 * Proporciona una forma de desacoplar el envío de tareas de la mecánica de cómo se ejecutará cada tarea.
 * Normalmente se utiliza un {@link Executor} en lugar de crear subprocesos explícitamente.
 */
@ApplicationScoped
public class AppExecutors {
    private final Executor diskIO;
    private final Executor mainThread;

    public AppExecutors(Executor diskIO, Executor mainThread) {
        this.diskIO = diskIO;
        this.mainThread = mainThread;
    }

    public Executor diskIO() {
        return diskIO;
    }

    public Executor mainThread() {
        return mainThread;
    }

    public static class MainThreadExecutor implements Executor {
        private final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(@NonNull Runnable command) {
            mainThreadHandler.post(command);
        }
    }
}
