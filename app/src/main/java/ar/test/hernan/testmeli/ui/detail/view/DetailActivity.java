package ar.test.hernan.testmeli.ui.detail.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import javax.inject.Inject;

import ar.test.hernan.testmeli.R;
import ar.test.hernan.testmeli.app.BaseActivity;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Vista detallada de un item seleccionado en {@link ar.test.hernan.testmeli.ui.items.view.ItemsActivity}
 * */
public class DetailActivity extends BaseActivity {
    @SuppressWarnings("unused") private static final String TAG = "DetailActivity";

    @Inject @SuppressWarnings("WeakerAccess")
    DetailFragment injectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_detail);
        this.initialize();
    }

    @Override
    public void initialize() {
        this.initFragment();
    }

    @Override
    public void initFragment() {
        FragmentManager manager = this.getSupportFragmentManager();
        checkNotNull(manager);
        DetailFragment fragment = (DetailFragment) manager.findFragmentById(R.id.detail_container);
        if (fragment == null) {
            fragment = injectedFragment;
            checkNotNull(fragment);
            manager.beginTransaction()
                    .add(R.id.detail_container, fragment)
                    .commit();
        }
    }
}
