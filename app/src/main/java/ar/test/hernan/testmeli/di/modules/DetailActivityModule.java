package ar.test.hernan.testmeli.di.modules;

import ar.test.hernan.testmeli.di.scopes.ActivityScoped;
import ar.test.hernan.testmeli.di.scopes.FragmentScoped;
import ar.test.hernan.testmeli.ui.detail.DetailContract;
import ar.test.hernan.testmeli.ui.detail.presenter.DetailPresenter;
import ar.test.hernan.testmeli.ui.detail.view.DetailFragment;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * {@link DetailActivityModule} contiene un modulo abstracto interno que enlaza {@link DetailContract.Presenter}
 * con {@link DetailFragment}
 */
@Module(includes = {DetailActivityModule.DetailAbstractModule.class})
class DetailActivityModule {
    //Utilizado por Dagger, no puede ser privado.
    @Module @SuppressWarnings({"unused", "WeakerAccess"})
    public abstract class DetailAbstractModule {
        @ActivityScoped
        @Binds
        abstract DetailContract.Presenter detailPresenter(DetailPresenter presenter);

        @FragmentScoped
        @ContributesAndroidInjector
        abstract DetailFragment detailFragment();
    }
}
