package ar.test.hernan.testmeli.di.modules;

import ar.test.hernan.testmeli.di.scopes.ActivityScoped;
import ar.test.hernan.testmeli.ui.detail.view.DetailActivity;
import ar.test.hernan.testmeli.ui.search.view.SearchActivity;
import ar.test.hernan.testmeli.ui.items.view.ItemsActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Es necesario que Dagger cree los subcomponentes de cada componente cualquiera sea el modulo.
 * Nunca necesitamos decirle a {@link ar.test.hernan.testmeli.di.AppComponent} que tendrá todos o
 * alguno de estos subcomponentes, ni necesitamos decirle a estos subcomponentes que
 * existe {@link ar.test.hernan.testmeli.di.AppComponent}.
 */
@Module @SuppressWarnings("unused")
public abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = SearchActivityModule.class)
     abstract SearchActivity searchActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = ItemsActivityModule.class)
    abstract ItemsActivity itemsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = DetailActivityModule.class)
    abstract DetailActivity detailActivity();
}
