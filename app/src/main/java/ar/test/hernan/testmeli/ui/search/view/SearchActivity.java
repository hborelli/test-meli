package ar.test.hernan.testmeli.ui.search.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import javax.inject.Inject;

import ar.test.hernan.testmeli.R;
import ar.test.hernan.testmeli.app.BaseActivity;

import static com.google.common.base.Preconditions.checkNotNull;

public class SearchActivity extends BaseActivity {
    @SuppressWarnings("unused") private static final String TAG = "SearchActivity";

    @Inject @SuppressWarnings("WeakerAccess")
    SearchFragment injectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_search);
        this.initialize();
    }

    @Override
    public void initialize() {
        this.initFragment();
    }

    @Override
    public void initFragment() {
        FragmentManager manager = this.getSupportFragmentManager();
        checkNotNull(manager);
        SearchFragment fragment = (SearchFragment) manager.findFragmentById(R.id.search_container);
        if (fragment == null) {
            fragment = injectedFragment;
            checkNotNull(fragment);
            manager.beginTransaction()
                    .add(R.id.search_container, fragment)
                    .commit();
        }
    }
}
