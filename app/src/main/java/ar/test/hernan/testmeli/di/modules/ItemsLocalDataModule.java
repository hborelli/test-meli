package ar.test.hernan.testmeli.di.modules;

import android.app.Application;
import android.arch.persistence.room.Room;

import ar.test.hernan.testmeli.data.source.local.ItemsDao;
import ar.test.hernan.testmeli.data.source.local.ItemsDataBase;
import ar.test.hernan.testmeli.di.scopes.ApplicationScoped;
import ar.test.hernan.testmeli.util.Constants;
import ar.test.hernan.testmeli.util.executorutils.AppExecutors;
import ar.test.hernan.testmeli.util.executorutils.DiskIOThreadExecutor;
import dagger.Module;
import dagger.Provides;

@Module @SuppressWarnings("WeakerAccess")
public class ItemsLocalDataModule {
    @ApplicationScoped
    @Provides
    ItemsDataBase provideDb(Application context) {
        return Room.databaseBuilder(context.getApplicationContext(),
                ItemsDataBase.class, Constants.ITEMS_ROOM_DB_STRING)
                .build();
    }

    @ApplicationScoped
    @Provides
    ItemsDao provideItemsDao(ItemsDataBase db) {
        return db.itemDao();
    }

    @ApplicationScoped
    @Provides
    AppExecutors provideAppExecutors() {
        return new AppExecutors(new DiskIOThreadExecutor(), new AppExecutors.MainThreadExecutor());
    }
}
