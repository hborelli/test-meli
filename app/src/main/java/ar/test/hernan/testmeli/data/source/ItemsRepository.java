package ar.test.hernan.testmeli.data.source;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import ar.test.hernan.testmeli.data.models.Item;
import ar.test.hernan.testmeli.data.source.scopes.Local;
import ar.test.hernan.testmeli.data.source.scopes.Remote;
import ar.test.hernan.testmeli.util.conectivityutils.OnlineChecker;
import io.reactivex.disposables.Disposable;

/**
 * Repositorio que maneja toda la lógica para la obtención de información referente a uno o varios {@link Item}
 * Las consultas se realizan de forma local o remota de forma transparente para el usuario.
 */
public class ItemsRepository implements ItemsDataSource {
    @SuppressWarnings("unused") private static final String TAG = "ItemsRepository";

    private final ItemsDataSource mItemsRemoteDataSource;
    private final ItemsDataSource mItemsLocalDataSource;
    private final OnlineChecker mOnlineChecker;

    @Inject
    ItemsRepository(
            @Remote ItemsDataSource itemsRemoteDataSource,
            @Local ItemsDataSource itemsLocalDataSource, OnlineChecker onlineChecker) {
        this.mItemsRemoteDataSource = itemsRemoteDataSource;
        this.mItemsLocalDataSource = itemsLocalDataSource;
        this.mOnlineChecker = onlineChecker;
    }

    /**
     * Verifica la conexion del usuario, si esta online, hace una consulta remota para obtener una lista de items en base a la
     * palabra clave buscada, si el usuario esta offline, verifica en la DB interna con resultados de busquedas anteriores si
     * es posible obtener una lista de items que satisfaga la busqueda realizada.
     *
     * @param search Palabra clave a buscar en {@link Item#title}.
     * @param limit Limite de {@link Item} devueltos en la consulta.
     * @param callback Para obtener cuando la consulta ha finalizado.
     * */
    @Override
    public void getItems(String search, int limit, @NonNull LoadItemsCallback callback) {
        if (this.mOnlineChecker.isOnline()) {
            this.getItemsFromRemoteDataSource(search, limit, callback);
        } else {
            this.getItemsFromLocalDataSource(search, limit, callback);
        }
    }

    /**
     * Consulta remota para obtener el listado de {@link Item} deseado. Si la consulta no produce resultados, realiza un ultimo
     * intento consultando la DB local.
     *
     * @param search Palabra clave a buscar en {@link Item#title}.
     * @param limit Limite de {@link Item} devueltos en la consulta.
     * @param callback Para obtener cuando la consulta ha finalizado.
     * */
    private void getItemsFromRemoteDataSource(String search, int limit, @NonNull final LoadItemsCallback callback){
        this.mItemsRemoteDataSource.getItems(search, limit, new LoadItemsCallback() {
            @Override
            public void onDisposableAcquired(Disposable disposable) {
                callback.onDisposableAcquired(disposable);
            }

            @Override
            public void onItemsLoaded(List<Item> items) {
                successfulItemsLoaded(items, callback);
            }

            @Override
            public void onDataNotAvailable() {
                getItemsFromLocalDataSource(search, limit, callback);
            }
        });
    }

    /**
     * Consulta local para obtener el listado de {@link Item} deseado.
     *
     * @param search Palabra clave a buscar en {@link Item#title}.
     * @param limit Limite de {@link Item} devueltos en la consulta.
     * @param callback Para obtener cuando la consulta ha finalizado.
     * */
    private void getItemsFromLocalDataSource(String search, int limit, @NonNull final LoadItemsCallback callback) {
        this.mItemsLocalDataSource.getItems(search, limit, new LoadItemsCallback() {
            @Override
            public void onDisposableAcquired(Disposable disposable) {
                callback.onDisposableAcquired(disposable);
            }

            @Override
            public void onItemsLoaded(List<Item> items) {
                callback.onItemsLoaded(items);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    /**
     * Si el listado de {@link Item} no es nulo y hay elementos en la lista,
     * la obtencion de el listado de {@link Item} fue exitosa, y se procede a actualizar la lista de items local.
     *
     * @param items Nuevo listado de items obtenidos
     * @param callback Para obtener la respuesta de la solicitud
     * */
    private void successfulItemsLoaded(List<Item> items, @NonNull LoadItemsCallback callback){
        if (items != null && items.size() > 0){
            callback.onItemsLoaded(items);
            this.refreshItems(items);
        }else {
            callback.onDataNotAvailable();
        }
    }

    /**
     * Se inserta el listado de nuevos {@link Item} obtenidos en la db local.
     *
     * @param items Items a agregar.
     * */
    private void refreshItems(List<Item> items) {
        for (Item singleItem : items) this.insertItem(singleItem);
    }

    /**
     * Inserta un solo {@link Item} en la DB local.
     * */
    @Override
    public void insertItem(@NonNull Item item) {
        this.mItemsLocalDataSource.insertItem(item);
    }
}
