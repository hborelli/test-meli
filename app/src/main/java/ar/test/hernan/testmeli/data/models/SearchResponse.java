package ar.test.hernan.testmeli.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Resultado de busqueda de <a href="https://api.mercadolibre.com/sites/MLA/search">https://api.mercadolibre.com/sites/MLA/search</a>
 * La key result contiene los resultados de busqueda de un producto o servicio en Mercado Libre.
 * */
public class SearchResponse {
    @SerializedName("results")
    @Expose
    public final List<Item> items;

    @SuppressWarnings("unused")
    public SearchResponse(List<Item> items) {
        this.items = items;
    }
}
