package ar.test.hernan.testmeli.util.conectivityutils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Custom {@link OnlineChecker} para poder invocar el metodo {@link #isOnline()} de forma sencilla.
 */
public class DefaultOnlineChecker implements OnlineChecker{
    private final ConnectivityManager connectivityManager;

    public DefaultOnlineChecker(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }

    @Override
    public boolean isOnline() {
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
