package ar.test.hernan.testmeli.di.modules;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

import com.squareup.picasso.Picasso;

import ar.test.hernan.testmeli.di.scopes.ApplicationScoped;
import ar.test.hernan.testmeli.util.conectivityutils.DefaultOnlineChecker;
import ar.test.hernan.testmeli.util.conectivityutils.OnlineChecker;
import dagger.Module;
import dagger.Provides;

@Module
public class UtilityModule {
    @Provides
    @ApplicationScoped
    ConnectivityManager provideConnectivityManager(Application context) {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Provides
    @ApplicationScoped
    OnlineChecker onlineChecker(ConnectivityManager cm) {
        return new DefaultOnlineChecker(cm);
    }

    @ApplicationScoped
    @Provides
    Picasso providePicasso(Application app) {
        return Picasso.with(app);
    }
}
