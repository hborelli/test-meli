package ar.test.hernan.testmeli.data.source.local;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import ar.test.hernan.testmeli.data.source.ItemsDataSource;
import ar.test.hernan.testmeli.data.models.Item;
import ar.test.hernan.testmeli.di.scopes.ApplicationScoped;
import ar.test.hernan.testmeli.util.executorutils.AppExecutors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Proveedor de informacion local referente a uno o varios {@link Item}
 * Las consultas se hacen a traves de {@link ItemsLocalDataSource} y {@link ar.test.hernan.testmeli.data.source.remote.ItemsRemoteDataSource}
 * de forma invisible para el usuario.
 * */
@ApplicationScoped
public class ItemsLocalDataSource implements ItemsDataSource {
    private final ItemsDao mItemsDao;
    private final AppExecutors mAppExecutors;

    @Inject
    public ItemsLocalDataSource(@NonNull AppExecutors executors, @NonNull ItemsDao itemsDao) {
        this.mItemsDao = itemsDao;
        this.mAppExecutors = executors;
    }

    /**
     * Obtiene una lista de items cuyo titulo contenga la palbra clave buscada. Acepta un limite de busqueda.
     *
     * @param search Palabra clave a buscar en {@link Item#title}.
     * @param limit Limite de {@link Item} devueltos en la consulta.
     * @param callback Para obtener cuando la consulta ha finalizado.
     * */
    @Override
    public void getItems(String search, int limit, @NonNull LoadItemsCallback callback) {
        Runnable runnable = () -> {
            final List<Item> items = this.mItemsDao.getItems(search);
            this.mAppExecutors.mainThread().execute(() -> {
                if (items.isEmpty()) {
                    callback.onDataNotAvailable();
                } else {
                    callback.onItemsLoaded(items);
                }
            });
        };
        this.mAppExecutors.diskIO().execute(runnable);
    }

    /**
     * Inserta un nuevo {@link Item} en la DB.
     *
     * @param item Para insertar en la DB local
     * */
    @Override
    public void insertItem(@NonNull final Item item) {
        checkNotNull(item);
        Runnable saveRunnable = () -> mItemsDao.insertItem(item);
        this.mAppExecutors.diskIO().execute(saveRunnable);
    }
}
