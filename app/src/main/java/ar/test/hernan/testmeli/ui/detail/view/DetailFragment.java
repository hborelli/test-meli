package ar.test.hernan.testmeli.ui.detail.view;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import ar.test.hernan.testmeli.R;
import ar.test.hernan.testmeli.app.BaseFragment;
import ar.test.hernan.testmeli.ui.detail.DetailContract;
import ar.test.hernan.testmeli.ui.detail.presenter.DetailPresenter;

/**
 * Vista detallada de un item seleccionado en {@link ar.test.hernan.testmeli.ui.items.view.ItemsActivity}
 * */
public class DetailFragment extends BaseFragment implements DetailContract.View{
    @SuppressWarnings("unused") private static final String TAG = "DetailFragment";

    @Inject @SuppressWarnings("WeakerAccess")
    DetailPresenter mPresenter;

    /*#UI*/
    private AppCompatImageView mDetailImage;
    private AppCompatTextView mTitleEditText;
    private AppCompatTextView mCategoryEditText;
    private AppCompatTextView mPriceEditText;
    private AppCompatTextView mAvailableQuantityEditText;

    @Inject
    public DetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        this.initialize(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.mPresenter.attach(this);
        this.mPresenter.viewResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mPresenter.detach();
    }

    private void initialize(View rootView) {
        this.mDetailImage = rootView.findViewById(R.id.detail_image);
        this.mTitleEditText = rootView.findViewById(R.id.detail_title_text_view);
        this.mCategoryEditText = rootView.findViewById(R.id.detail_category_text_view);
        this.mPriceEditText = rootView.findViewById(R.id.detail_price_text_view);
        this.mAvailableQuantityEditText = rootView.findViewById(R.id.detail_available_quantity_text_view);
    }

    /*#Getters and setters*/
    @Override
    public DetailPresenter getPresenter() {
        return this.mPresenter;
    }
    @Override
    public AppCompatImageView getDetailImageView() {
        return this.mDetailImage;
    }
    @Override
    public void setTitle(String title){
        this.mTitleEditText.setText(title);
    }
    @Override
    public void setPrice(String price){
        this.mPriceEditText.setText(price);
    }
    @Override
    public void setCategory(String category){
        this.mCategoryEditText.setText(category);
    }
    @Override
    public void setAvailableQuantity(String quantity){
        this.mAvailableQuantityEditText.setText(quantity);
    }
}
