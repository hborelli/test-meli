package ar.test.hernan.testmeli.util;

/**
 * Keys y variables de uso global
 * */
public class Constants {
    /*#API*/
    public static final String API_BASE_URL = "https://api.mercadolibre.com/sites/";
    public static final String REGION = "MLA/";
    public static final String ITEMS_ROOM_DB_STRING = "Items.db";

    /*#Keys*/
    public static final String SEARCH_EXTRA_KEY = "SEARCH_KEY";
    public static final String ITEM_EXTRA_KEY = "ITEM_KEY";
}
