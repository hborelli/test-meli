package ar.test.hernan.testmeli.ui.items.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import javax.inject.Inject;

import ar.test.hernan.testmeli.R;
import ar.test.hernan.testmeli.app.BaseFragment;
import ar.test.hernan.testmeli.ui.items.ItemsContract;
import ar.test.hernan.testmeli.ui.items.presenter.ItemsPresenter;
import ar.test.hernan.testmeli.util.conectivityutils.OnlineChecker;

/**
 * Un listado de {@link ar.test.hernan.testmeli.data.models.Item} relacionados a la palabra clave buscada por el
 * usuario en {@link ar.test.hernan.testmeli.ui.search.view.SearchActivity}
 * */
public class ItemsFragment extends BaseFragment implements ItemsContract.View{
    @SuppressWarnings("unused") private static final String TAG = "ItemsFragment";

    @Inject @SuppressWarnings("WeakerAccess")
    ItemsPresenter mPresenter;
    @Inject @SuppressWarnings("WeakerAccess")
    OnlineChecker onlineChecker;

    /*#UI*/
    private ListView mItemsRecyclerView;
    private ProgressBar mLoadingIndicator;

    @Inject
    public ItemsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search_result, container, false);
        this.initialize(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.mPresenter.attach(this);
        this.mPresenter.viewResume();
        if (!onlineChecker.isOnline()) showOfflineStateMessage();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mPresenter.detach();
    }

    private void initialize(View rootView){
        this.mItemsRecyclerView = rootView.findViewById(R.id.search_result_recycler_view);
        this.mLoadingIndicator = rootView.findViewById(R.id.search_result_loading);
        this.setListeners();
    }

    private void setListeners() {
        this.mItemsRecyclerView.setOnItemClickListener((adapterView, view, i, l) -> mPresenter.itemSelected(i));
    }

    public void showLoading(){
        this.mLoadingIndicator.setVisibility(View.VISIBLE);
    }

    public void hideLoading(){
        this.mLoadingIndicator.setVisibility(View.INVISIBLE);
    }

    /*#Getters and setters*/
    @Override
    public ItemsPresenter getPresenter() {
        return mPresenter;
    }
    public void setAdapter(ItemViewAdapter adapter){
        this.mItemsRecyclerView.setAdapter(adapter);
    }
}
