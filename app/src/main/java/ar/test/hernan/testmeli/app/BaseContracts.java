package ar.test.hernan.testmeli.app;

import android.content.Context;

import io.reactivex.annotations.NonNull;

/**
 * Contrato base entre las views y los presenters.
 * */
public interface BaseContracts {
    interface View<T extends BaseContracts.Presenter> {
        @NonNull Context getContext();
        T getPresenter();
    }

    abstract class Presenter<T extends BaseContracts.View> {
        protected T view;
        public void attach(T view) {
            this.view = view;
        }
        public void detach() {
            this.view = null;
        }
        public T getView() {
            return view;
        }
        public abstract void viewResume();
    }
}
