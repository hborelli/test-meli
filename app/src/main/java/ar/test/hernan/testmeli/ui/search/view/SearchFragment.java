package ar.test.hernan.testmeli.ui.search.view;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import ar.test.hernan.testmeli.R;
import ar.test.hernan.testmeli.app.BaseFragment;
import ar.test.hernan.testmeli.ui.search.SearchContract;
import ar.test.hernan.testmeli.ui.search.presenter.SearchPresenter;

public class SearchFragment extends BaseFragment implements SearchContract.View {
    @SuppressWarnings("unused") private static final String TAG = "SearchFragment";

    @Inject @SuppressWarnings("WeakerAccess")
    SearchPresenter mPresenter;

    /*#UI*/
    private AppCompatEditText mSearchEditText;
    private AppCompatButton mSearchButton;

    @Inject
    public SearchFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        this.initialize(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.attach(this);
        mPresenter.viewResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detach();
    }

    private void initialize(View rootView){
        this.mSearchEditText = rootView.findViewById(R.id.search_input);
        this.mSearchButton = rootView.findViewById(R.id.search_button);
        this.setListeners();
    }

    private void setListeners() {
        this.mSearchButton.setOnClickListener(view -> mPresenter.search());
    }

    /*#Getters and setters*/
    @Override
    public SearchPresenter getPresenter() {
        return mPresenter;
    }
    @Override
    public AppCompatEditText getSearchEditText(){
        return this.mSearchEditText;
    }
}
