package ar.test.hernan.testmeli.ui.search;

import android.support.v7.widget.AppCompatEditText;

import ar.test.hernan.testmeli.app.BaseContracts;
import ar.test.hernan.testmeli.ui.search.presenter.SearchPresenter;
import ar.test.hernan.testmeli.ui.search.view.SearchFragment;

/**
 * Contrato con el comportamiento esperado entre {@link SearchFragment} y {@link SearchPresenter}
 * */
public interface SearchContract {
    interface View extends BaseContracts.View<SearchPresenter> {
        AppCompatEditText getSearchEditText();
    }

    abstract class Presenter extends BaseContracts.Presenter<SearchFragment> {
        public abstract void search();
    }
}
