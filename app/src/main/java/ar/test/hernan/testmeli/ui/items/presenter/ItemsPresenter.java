package ar.test.hernan.testmeli.ui.items.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import ar.test.hernan.testmeli.R;
import ar.test.hernan.testmeli.data.models.Item;
import ar.test.hernan.testmeli.data.source.ItemsDataSource;
import ar.test.hernan.testmeli.data.source.ItemsRepository;
import ar.test.hernan.testmeli.di.scopes.ActivityScoped;
import ar.test.hernan.testmeli.ui.detail.view.DetailActivity;
import ar.test.hernan.testmeli.ui.items.ItemsContract;
import ar.test.hernan.testmeli.ui.items.view.ItemViewAdapter;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static ar.test.hernan.testmeli.util.Constants.SEARCH_EXTRA_KEY;
import static ar.test.hernan.testmeli.util.Constants.ITEM_EXTRA_KEY;

/**
 * Obtiene la informacion requerida y la muestra a traves de {@link ar.test.hernan.testmeli.ui.items.view.ItemsFragment}
 * */
@ActivityScoped
public class ItemsPresenter extends ItemsContract.Presenter {
    @SuppressWarnings("unused") private static final String TAG = "ItemsPresenter";

    @Inject @SuppressWarnings("WeakerAccess")
    Picasso mPicasso;
    /*#Data*/
    private final ItemsRepository mItemsRepository;
    private final CompositeDisposable mDisposables;
    @Nullable
    private List<Item> mSearchResult;

    /*#Constantes*/
    private static final int ITEMS_PER_PAGE = 10;

    @Inject
    ItemsPresenter(ItemsRepository newsRepository, CompositeDisposable disposable) {
        this.mDisposables = disposable;
        this.mItemsRepository = newsRepository;
    }

    @Override
    public void viewResume() {
        this.searchItems();
    }

    @Override
    public void detach() {
        this.mDisposables.clear();
        this.mDisposables.dispose();
    }

    /**
     * Obtiene la palabra clave ingresada por el usuario en {@link ar.test.hernan.testmeli.ui.search.view.SearchActivity}
     * y llama a {@link #searchItems(String)}.
     * */
    private void searchItems() {
        if (this.getView().getActivity() != null && this.getView().getActivity().getIntent() != null){
            String search = this.getView().getActivity().getIntent().getStringExtra(SEARCH_EXTRA_KEY);
            this.searchItems(search);
        }
    }

    /**
     * Realiza una solicitud para obtener los {@link Item} relacionados a la palabra clave ingresada por el usuario.
     *
     * @param search Ingresada previamente en {@link ar.test.hernan.testmeli.ui.search.view.SearchActivity}
     * */
    private void searchItems(String search){
        this.view.showLoading();
        this.mItemsRepository.getItems(search, ITEMS_PER_PAGE, new ItemsDataSource.LoadItemsCallback() {
            @Override
            public void onDisposableAcquired(Disposable disposable) {

            }

            @Override
            public void onItemsLoaded(List<Item> items) {
                successfulGetItems(items);
            }

            @Override
            public void onDataNotAvailable() {
                withoutResults();
            }
        });
    }

    /**
     * La solicitud del listado de {@link Item} fue exitosa.
     * Se actualizar la lista de items.
     *
     * @param items Respuesta de la solicitud
     * */
    private void successfulGetItems(List<Item> items) {
        this.view.hideLoading();
        this.mSearchResult = items;
        if (view.getContext() != null) this.initAdapter(view.getContext());
    }

    /**
     * Inicia el adapter para mostrar la lista de items.
     *
     * @param context Contexto actual de la aplicacion.
     * */
    private void initAdapter(@NonNull Context context){
        ItemViewAdapter adapter = new ItemViewAdapter(context, this);
        this.view.setAdapter(adapter);
    }

    /**
     * La solicitud de un listado de {@link Item} no produjo ningun resultado, se muestra un mensaje en pantalla
     * para comunicarselo al usuario.
     * */
    private void withoutResults(){
        this.view.hideLoading();
        if(this.view.getContext() != null) this.view.showMessage(view.getContext().getString(R.string.without_results));
    }

    /**
     * Un item fue seleccionado para ver su detalle. Se llama a {@link #goToDetail(Item)}
     *
     * @param position posicion en el ListView.
     * */
    @Override
    public void itemSelected(int position) {
        if (this.mSearchResult!= null && this.mSearchResult.size() > position){
            Item item = this.mSearchResult.get(position);
            this.goToDetail(item);
        }
    }

    /**
     * Carga la imagen previa de {@link Item#thumbnail}
     *
     * @param position Posicion en el array del item.
     * @param imageView Donde la imagen sera mostrada.
     * */
    @Override
    public void loadImage(int position, AppCompatImageView imageView){
        if (this.mSearchResult != null
                && this.mSearchResult.size() > position
                && this.mSearchResult.get(position) != null)
            this.mPicasso
                    .load(this.mSearchResult.get(position).getThumbnail())
                    .resize(150, 150)
                    .centerInside()
                    .into(imageView);
    }

    /**
     * Transición de activity, para ver el detalle de un item.
     * */
    private void goToDetail(@NonNull Item item){
        Intent intent = new Intent(this.view.getContext(), DetailActivity.class);
        intent.putExtra(ITEM_EXTRA_KEY, item);
        if(this.view.getContext() != null) this.view.getContext().startActivity(intent);
    }

    /*#Getters and setters*/
    @Override
    public int getCount(){
        return (this.mSearchResult != null) ? this.mSearchResult.size() : 0;
    }
    @NonNull @Override
    public String getItemTitle(int position){
        if (this.mSearchResult != null
                && this.mSearchResult.size() > position
                && this.mSearchResult.get(position) != null){
            return this.mSearchResult.get(position).getTitle();
        }else {
            return "";
        }
    }
    @NonNull @Override
    public String getItemPrice(int position){
        if (this.mSearchResult != null
                && this.mSearchResult.size() > position
                && this.mSearchResult.get(position) != null){
            return String.format(Locale.getDefault(), "$ %.2f", this.mSearchResult.get(position).getPrice());
        }else {
            return "";
        }
    }
}
