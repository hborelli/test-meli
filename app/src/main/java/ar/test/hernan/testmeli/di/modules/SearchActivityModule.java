package ar.test.hernan.testmeli.di.modules;

import ar.test.hernan.testmeli.di.scopes.ActivityScoped;
import ar.test.hernan.testmeli.di.scopes.FragmentScoped;
import ar.test.hernan.testmeli.ui.search.SearchContract;
import ar.test.hernan.testmeli.ui.search.presenter.SearchPresenter;
import ar.test.hernan.testmeli.ui.search.view.SearchFragment;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * {@link SearchActivityModule} contiene un modulo abstracto interno que enlaza {@link SearchContract.Presenter}
 * con {@link SearchFragment}
 */
@Module(includes = {SearchActivityModule.SearchAbstractModule.class})
class SearchActivityModule {
    //Utilizado por Dagger, no puede ser privado.
    @Module @SuppressWarnings({"unused", "WeakerAccess"})
    public abstract class SearchAbstractModule {
        @ActivityScoped
        @Binds
        abstract SearchContract.Presenter searchPresenter(SearchPresenter presenter);

        @FragmentScoped
        @ContributesAndroidInjector
        abstract SearchFragment searchFragment();
    }
}
