package ar.test.hernan.testmeli.data.source.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Scope creado para Dagger, para diferenciar entre los dos tipos de proveedores de informacion utilizados en la aplicacion.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface Local {
}
