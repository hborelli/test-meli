package ar.test.hernan.testmeli.ui.items;

import android.support.v7.widget.AppCompatImageView;

import ar.test.hernan.testmeli.app.BaseContracts;
import ar.test.hernan.testmeli.ui.items.presenter.ItemsPresenter;
import ar.test.hernan.testmeli.ui.items.view.ItemsFragment;

/**
 * Contrato con el comportamiento esperado entre {@link ItemsFragment} y {@link ItemsPresenter}
 * */
public interface ItemsContract {
    interface View extends BaseContracts.View<ItemsPresenter> {
        void showLoading();
        void hideLoading();
    }

    abstract class Presenter extends BaseContracts.Presenter<ItemsFragment> {
        public abstract void itemSelected(int position);
        public abstract void loadImage(int position, AppCompatImageView imageView);
        public abstract int getCount();
        public abstract String getItemTitle(int position);
        public abstract String getItemPrice(int position);
    }
}
