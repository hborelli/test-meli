package ar.test.hernan.testmeli.data.source.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import ar.test.hernan.testmeli.data.models.Item;

/**
 * Data access object interface para las consultas locales referentes a una lista de {@link Item}
 * Se pueden hacer consultas sin conexion sobre {@link Item} previamente buscados.
 * */
@Dao
public interface ItemsDao {
    /**
     * Busca en la DB intera {@link Item} que contengan la palabra buscada.
     *
     * @param search Palabra clave a buscar en el titulo de los {@link Item}
     * @return Lista de items que contengan la palabra clave buscada.
     * */
    @Query("SELECT * FROM items WHERE title LIKE '%' || :search || '%'")
    List<Item> getItems(String search);

    /**
     * Busca en la DB intera {@link Item} por id.
     *
     * @param id Item id
     * @return Item con el id seleccionada
     * */
    @Query("SELECT * FROM items WHERE id = :id")
    Item getItem(String id);

    /**
     * Inserta un nuevo {@link Item} en la DB
     *
     * @param item Nuevo {@link Item} para insertar en la DB
     * */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertItem(Item item);
}
