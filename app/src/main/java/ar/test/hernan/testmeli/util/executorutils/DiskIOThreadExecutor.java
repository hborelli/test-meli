package ar.test.hernan.testmeli.util.executorutils;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.reactivex.annotations.NonNull;

/**
 * Ejecuta una tarea en un nuevo hilo de fondo.
 * */
public class DiskIOThreadExecutor implements Executor {
    private final Executor mDiskIO;

    public DiskIOThreadExecutor() {
        mDiskIO = Executors.newSingleThreadExecutor();
    }

    @Override
    public void execute(@NonNull Runnable runnable) {
        mDiskIO.execute(runnable);
    }
}
