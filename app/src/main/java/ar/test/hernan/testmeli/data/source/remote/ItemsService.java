package ar.test.hernan.testmeli.data.source.remote;

import ar.test.hernan.testmeli.data.models.SearchResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Consultas remotas referentes a uno o varios {@link ar.test.hernan.testmeli.data.models.Item}
 * */
public interface ItemsService {
    @GET("search")
    Single<SearchResponse> getItems(@Query("q") String search, @Query("limit") int limit);
}
