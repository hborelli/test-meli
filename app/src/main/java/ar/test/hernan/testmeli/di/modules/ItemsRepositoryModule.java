package ar.test.hernan.testmeli.di.modules;

import ar.test.hernan.testmeli.data.source.ItemsDataSource;
import ar.test.hernan.testmeli.data.source.local.ItemsDao;
import ar.test.hernan.testmeli.data.source.local.ItemsLocalDataSource;
import ar.test.hernan.testmeli.data.source.remote.ItemsRemoteDataSource;
import ar.test.hernan.testmeli.data.source.remote.ItemsService;
import ar.test.hernan.testmeli.data.source.scopes.Local;
import ar.test.hernan.testmeli.data.source.scopes.Remote;
import ar.test.hernan.testmeli.di.scopes.ApplicationScoped;
import ar.test.hernan.testmeli.util.executorutils.AppExecutors;
import dagger.Module;
import dagger.Provides;

/**
 * {@link ItemsRepositoryModule} Contiene módulos de orígene de datos locales y remotos.
 */
@Module(includes = {ItemsRemoteDataModule.class, ItemsLocalDataModule.class})
public class ItemsRepositoryModule {
    @Provides
    @Local
    @ApplicationScoped
    ItemsDataSource provideItemsLocalDataSource(AppExecutors appExecutors, ItemsDao newsDao) {
        return new ItemsLocalDataSource(appExecutors, newsDao);
    }

    @Provides
    @Remote
    @ApplicationScoped
    ItemsDataSource provideItemsRemoteDataSource(ItemsService itemsService) {
        return new ItemsRemoteDataSource(itemsService);
    }
}
