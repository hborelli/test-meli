package ar.test.hernan.testmeli.ui.items.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import javax.inject.Inject;

import ar.test.hernan.testmeli.R;
import ar.test.hernan.testmeli.app.BaseActivity;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Un listado de {@link ar.test.hernan.testmeli.data.models.Item} relacionados a la palabra clave buscada por el
 * usuario en {@link ar.test.hernan.testmeli.ui.search.view.SearchActivity}
 * */
public class ItemsActivity extends BaseActivity {
    @SuppressWarnings("unused") private static final String TAG = "ItemsActivity";

    @Inject @SuppressWarnings("WeakerAccess")
    ItemsFragment injectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_search_result);
        this.initialize();
    }

    @Override
    public void initialize() {
        this.initFragment();
    }

    @Override
    public void initFragment() {
        FragmentManager manager = this.getSupportFragmentManager();
        checkNotNull(manager);
        ItemsFragment fragment = (ItemsFragment) manager.findFragmentById(R.id.search_result_container);
        if (fragment == null) {
            fragment = injectedFragment;
            checkNotNull(fragment);
            manager.beginTransaction()
                    .add(R.id.search_result_container, fragment)
                    .commit();
        }
    }
}
