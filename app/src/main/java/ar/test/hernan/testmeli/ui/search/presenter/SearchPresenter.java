package ar.test.hernan.testmeli.ui.search.presenter;

import android.content.Intent;

import javax.inject.Inject;

import ar.test.hernan.testmeli.di.scopes.ActivityScoped;
import ar.test.hernan.testmeli.ui.search.SearchContract;
import ar.test.hernan.testmeli.ui.items.view.ItemsActivity;

import static ar.test.hernan.testmeli.util.Constants.SEARCH_EXTRA_KEY;

/**
 * Logica de transicion entre {@link ar.test.hernan.testmeli.ui.search.view.SearchActivity} y {@link ItemsActivity}
 * */
@ActivityScoped
public class SearchPresenter extends SearchContract.Presenter{
    @SuppressWarnings("unused") private static final String TAG = "SearchPresenter";

    @Inject
    SearchPresenter() {
        //Empty constructor, requerido.
    }

    @Override
    public void viewResume() {

    }

    @Override
    public void detach() {

    }

    /**
     * Obtiene la palabra clave a buscar ingresada por el usuario y llama {@link #goToItems(String)}
     * */
    @Override
    public void search() {
        if (this.view.getSearchEditText() != null && this.view.getSearchEditText().getText() != null){
            String search = this.view.getSearchEditText().getText().toString();
            this.goToItems(search);
        }
    }

    /**
     * Transición de activity, para la lista de {@link ar.test.hernan.testmeli.data.models.Item} relacionados a la
     * palabra clave buscada.
     * */
    private void goToItems(String search){
        Intent intent = new Intent(this.view.getContext(), ItemsActivity.class);
        intent.putExtra(SEARCH_EXTRA_KEY, search);
        if(this.view.getContext() != null) this.view.getContext().startActivity(intent);
    }
}
