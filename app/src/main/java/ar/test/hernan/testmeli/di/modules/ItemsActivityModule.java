package ar.test.hernan.testmeli.di.modules;

import ar.test.hernan.testmeli.di.scopes.ActivityScoped;
import ar.test.hernan.testmeli.di.scopes.FragmentScoped;
import ar.test.hernan.testmeli.ui.items.ItemsContract;
import ar.test.hernan.testmeli.ui.items.presenter.ItemsPresenter;
import ar.test.hernan.testmeli.ui.items.view.ItemsFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import io.reactivex.disposables.CompositeDisposable;

/**
 * {@link ItemsActivityModule} contiene un modulo abstracto interno que enlaza {@link ItemsContract.Presenter}
 * con {@link ItemsFragment}
 */
@Module(includes = {ItemsActivityModule.ItemsAbstractModule.class})
public class ItemsActivityModule {
    @ActivityScoped
    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    //Utilizado por Dagger, no puede ser privado.
    @Module @SuppressWarnings({"unused", "WeakerAccess"})
    public abstract class ItemsAbstractModule {
        @ActivityScoped
        @Binds
        abstract ItemsContract.Presenter searchResultPresenter(ItemsPresenter presenter);

        @FragmentScoped
        @ContributesAndroidInjector
        abstract ItemsFragment searchResultFragment();
    }
}
