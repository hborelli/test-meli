package ar.test.hernan.testmeli.ui.detail.presenter;

import android.support.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.Locale;

import javax.inject.Inject;

import ar.test.hernan.testmeli.data.models.Item;
import ar.test.hernan.testmeli.ui.detail.DetailContract;

import static ar.test.hernan.testmeli.util.Constants.ITEM_EXTRA_KEY;

/**
 * Obtiene la informacion requerida y la muestra a traves de {@link ar.test.hernan.testmeli.ui.detail.view.DetailFragment}
 * */
public class DetailPresenter extends DetailContract.Presenter{
    @SuppressWarnings("unused") private static final String TAG = "DetailPresenter";

    @Inject @SuppressWarnings("WeakerAccess")
    Picasso mPicasso;

    @Inject
    DetailPresenter() {
        //Empty constructor, requerido.
    }

    @Override
    public void viewResume() {
        this.getSelectedItem();
    }

    @Override
    public void detach() {

    }

    /**
     * Busca el {@link Item} seleccionado en {@link ar.test.hernan.testmeli.ui.items.view.ItemsActivity}
     * y llama a {@link #showDetails(Item)}
     * */
    private void getSelectedItem(){
        if (this.getView().getActivity() != null && this.getView().getActivity().getIntent() != null){
            Item item = (Item) this.getView().getActivity().getIntent().getSerializableExtra(ITEM_EXTRA_KEY);
            this.showDetails(item);
        }
    }

    /**
     * Muestra la informacion relacionada al {@link Item} seleccionado
     *
     * @param item Seleccionado previamente en {@link ar.test.hernan.testmeli.ui.items.view.ItemsActivity}
     * */
    private void showDetails(@Nullable Item item){
        if (item != null){
            this.loadImage(item.getThumbnail());
            this.view.setTitle(item.getTitle());
            this.view.setCategory(item.getCategory());
            this.view.setPrice(String.format(Locale.getDefault(), "$ %.2f", item.getPrice()));
            this.view.setAvailableQuantity("" + item.getAvailableQuantity());
        }
    }

    /**
     * Muestra la imagen de {@link Item#thumbnail}
     * */
    private void loadImage(@Nullable String image){
        if (image != null){
            this.mPicasso
                    .load(image)
                    .resize(500, 500)
                    .centerInside()
                    .into(this.view.getDetailImageView());
        }
    }
}
