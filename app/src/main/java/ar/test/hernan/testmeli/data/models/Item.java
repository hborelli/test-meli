package ar.test.hernan.testmeli.data.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Representacion de un item singular resultante de una busqueda de un producto o servicio en Mercado Libre.
 * El {@link #thumbnail} es un link a una preview image.
 * */
@Entity(tableName = "items")
public class Item implements Serializable {
    @PrimaryKey
    @ColumnInfo(name = "id")
    @NonNull
    private final String id;

    @ColumnInfo(name = "source")
    private String sourceDataString;

    @SerializedName("site_id")
    @Expose
    @ColumnInfo(name = "site_id")
    private String siteId;

    @SerializedName("title")
    @Expose
    @ColumnInfo(name = "title")
    private final String title;

    @SerializedName("price")
    @Expose
    @ColumnInfo(name = "price")
    private final Float price;

    @SerializedName("available_quantity")
    @Expose
    @ColumnInfo(name = "available_quantity")
    private int availableQuantity;

    @SerializedName("thumbnail")
    @Expose
    @ColumnInfo(name = "thumbnail")
    private String thumbnail;

    @ColumnInfo(name = "category")
    private final String category;

    public Item(@NonNull String id, String title, Float price, int availableQuantity, String thumbnail, String category) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.availableQuantity = availableQuantity;
        this.category = category;
        this.thumbnail = thumbnail;
    }

    @Ignore
    public Item(@NonNull String id, String title, Float price, String category) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.category = category;
    }

    @NonNull @Override
    public String toString() {
        return (title + price);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return com.google.common.base.Objects.equal(id, item.id) &&
                com.google.common.base.Objects.equal(title, item.title) &&
                com.google.common.base.Objects.equal(availableQuantity, item.availableQuantity) &&
                com.google.common.base.Objects.equal(category, item.category) &&
                com.google.common.base.Objects.equal(thumbnail, item.thumbnail);
    }

    /*#Getters and setters*/
    public String getSiteId() {
        return siteId;
    }
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }
    public String getTitle() {
        return title;
    }
    public Float getPrice() {
        return price;
    }
    public int getAvailableQuantity() {
        return availableQuantity;
    }
    public String getSourceDataString() {
        return sourceDataString;
    }
    public void setSourceDataString(String sourceDataString) {
        this.sourceDataString = sourceDataString;
    }
    public String getCategory() {
        return category;
    }
    @NonNull
    public String getId() {
        return id;
    }
    public String getThumbnail() {
        return thumbnail;
    }
}
