package ar.test.hernan.testmeli.util.conectivityutils;

/**
 * Interfaz simple que contiene un indicador para saber si el usuario esta online u offline.
 */
public interface OnlineChecker {
    boolean isOnline();
}
