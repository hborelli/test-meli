package ar.test.hernan.testmeli.app;

import ar.test.hernan.testmeli.di.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * Se utiliza una clase Application custom, para poder extender de {@link DaggerApplication}
 * para poder inyectar las dependencias. Se sobreescribe {@link #applicationInjector()} para decirle a
 * Dagger como hacer el componente {@link ar.test.hernan.testmeli.di.scopes.ApplicationScoped}.
 * No es necesario de esta forma llamar nunca `component.inject(this)`, ya que {@link DaggerApplication} lo hará.
 * */
public class Application extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}
