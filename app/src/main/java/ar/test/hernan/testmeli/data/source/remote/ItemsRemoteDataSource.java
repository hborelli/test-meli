package ar.test.hernan.testmeli.data.source.remote;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import ar.test.hernan.testmeli.data.source.ItemsDataSource;
import ar.test.hernan.testmeli.data.models.Item;
import ar.test.hernan.testmeli.di.scopes.ApplicationScoped;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Proveedor de informacion remota referente a uno o varios {@link Item}
 * Las consultas se hacen a traves de {@link ar.test.hernan.testmeli.data.source.local.ItemsLocalDataSource} y {@link ItemsRemoteDataSource}
 * de forma invisible para el usuario.
 * */
@ApplicationScoped
public class ItemsRemoteDataSource implements ItemsDataSource {
    private final ItemsService mItemsService;

    @Inject
    public ItemsRemoteDataSource(ItemsService itemsService) {
        this.mItemsService = itemsService;
    }

    /**
     * Hace una consulta a la API de Mercado Libre para obtener una lista de {@link Item} cuyo titulo contenga la palabra
     * clave buscada. Acepta un limite de busqueda.
     *
     * @param search Palabra clave a buscar en {@link Item#title}.
     * @param limit Limite de {@link Item} devueltos en la consulta.
     * @param callback Para obtener cuando la consulta ha finalizado.
     * */
    @Override
    public void getItems(String search, int limit, @NonNull LoadItemsCallback callback) {
        this.mItemsService.getItems(search, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(searchResponse -> searchResponse.items)
                .subscribe(new SingleObserver<List<Item>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        callback.onDisposableAcquired(d);
                    }

                    @Override
                    public void onSuccess(List<Item> items) {
                        callback.onItemsLoaded(items);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onDataNotAvailable();
                    }
                });
    }

    @Override
    public void insertItem(@NonNull Item item) {
        //Dar de alta un nuevo producto en ML, no es requerido en este ejemplo. Es utilizado para agregar un item a la DB local
        //en ItemLocalDataSource.
    }
}
