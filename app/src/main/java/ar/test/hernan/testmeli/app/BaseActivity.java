package ar.test.hernan.testmeli.app;

import android.os.Bundle;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * Se utiliza una custom Activity para implementar metodos comunes entre todas las activities y
 * extender de {@link DaggerAppCompatActivity}, para inyectar dependencias en {@link #onCreate(Bundle)}
 * y los {@link android.support.v4.app.FragmentActivity} adjuntos.
 * */
public abstract class BaseActivity extends DaggerAppCompatActivity {
    public abstract void initialize();
    public abstract void initFragment();
}
