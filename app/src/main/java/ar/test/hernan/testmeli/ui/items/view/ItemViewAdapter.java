package ar.test.hernan.testmeli.ui.items.view;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import ar.test.hernan.testmeli.R;
import ar.test.hernan.testmeli.ui.items.ItemsContract;

/**
 * Celda simple que muestra la vista previa de un solo {@link ar.test.hernan.testmeli.data.models.Item}
 * */
public class ItemViewAdapter extends BaseAdapter {
    @SuppressWarnings("unused") private static final String TAG = "ItemViewAdapter";

    private final ItemsContract.Presenter mPresenter;
    private final LayoutInflater mInflater;

    /*#Holder*/
    class ViewHolder extends RecyclerView.ViewHolder {
        final AppCompatImageView mPreviewImage;
        final AppCompatTextView mTitle;
        final AppCompatTextView mPrice;

        ViewHolder(View view) {
            super(view);
            this.mPreviewImage = view.findViewById(R.id.item_cell_preview_image);
            this.mTitle = view.findViewById(R.id.item_cell_title);
            this.mPrice = view.findViewById(R.id.item_cell_price);
        }
    }

    public ItemViewAdapter(Context context, ItemsContract.Presenter presenter) {
        this.mPresenter = presenter;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = this.inflateView(view, viewGroup);
        this.initialize(view, i);
        return view;
    }

    private View inflateView(View view, ViewGroup viewGroup){
        if (view == null) return this.mInflater.inflate(R.layout.fragment_item, viewGroup, false);
        return view;
    }

    private void initialize(View view, int position) {
        ViewHolder holder = new ViewHolder(view);
        holder.mTitle.setText(this.mPresenter.getItemTitle(position));
        holder.mPrice.setText(this.mPresenter.getItemPrice(position));
        this.mPresenter.loadImage(position, holder.mPreviewImage);
    }

    /*#Getters and setters*/
    @Override
    public int getCount() {
        return mPresenter.getCount();
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return i;
    }
}
