package ar.test.hernan.testmeli.di;

import ar.test.hernan.testmeli.app.Application;
import ar.test.hernan.testmeli.di.modules.ActivityBindingModule;
import ar.test.hernan.testmeli.di.modules.ItemsRepositoryModule;
import ar.test.hernan.testmeli.di.modules.UtilityModule;
import ar.test.hernan.testmeli.di.scopes.ApplicationScoped;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * Este es un componente de raiz de Dagger
 * {@link AndroidSupportInjectionModule}
 * Ayuda con la generacion y ubicacion de subcomponentes, que pueden ser en este caso tambien activities.
 */
@ApplicationScoped
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ItemsRepositoryModule.class,
        UtilityModule.class,
        ActivityBindingModule.class
})
public interface AppComponent extends AndroidInjector<Application> {
    // Ahora podemos hacer DaggerAppComponent.builder().application(this).build().inject(this),
    // Nunca tendremos que crear una instancia de ningún módulo o decir a qué módulo estamos pasando la aplicación
    @Component.Builder
    interface Builder {
        @BindsInstance
        AppComponent.Builder application(android.app.Application application);
        AppComponent build();
    }
}
