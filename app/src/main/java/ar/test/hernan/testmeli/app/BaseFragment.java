package ar.test.hernan.testmeli.app;

import android.content.Context;
import android.support.design.widget.Snackbar;

import ar.test.hernan.testmeli.R;
import dagger.android.support.DaggerFragment;

/**
 * Se utiliza un custom Fragment para implementar metodos comunes entre todos los Fragments y extender de {@link DaggerFragment}
 * para inyectar dependencias en {@link #onAttach(Context)}.
 * */
public abstract class BaseFragment extends DaggerFragment {
    /**
     * Llama a {@link #showMessage(String)} con un mensaje predeterminado para comunicarle al usuario que no tiene
     * acceso a internet.
     * */
    protected void showOfflineStateMessage(){
        this.showMessage(this.getString(R.string.offline_message));
    }
    /**
     * Muestra un alerta con un mensaje
     * @param message Mensaje en pantalla.
     * */
    public void showMessage(String message) {
        if (this.getView() != null) Snackbar.make(this.getView(), message, Snackbar.LENGTH_LONG).show();
    }
}
