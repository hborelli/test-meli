package ar.test.hernan.testmeli.data.source;

import android.support.annotation.NonNull;

import java.util.List;

import ar.test.hernan.testmeli.data.models.Item;
import io.reactivex.disposables.Disposable;

/**
 * Repository interface. Para hacer consultas referentes a uno o varios {@link Item}
 * tanto locales como remotas utilizando los mismos metodos y que sea un proceso invisible para el usuario.
 * {@link LoadItemsCallback} para obtener los resultados de una consulta de una lista de {@link Item}
 * */
public interface ItemsDataSource {

    interface LoadItemsCallback {
        void onDisposableAcquired(Disposable disposable);
        void onItemsLoaded(List<Item> items);
        void onDataNotAvailable();
    }

    void getItems(String search, int limit, @NonNull LoadItemsCallback callback);
    void insertItem(@NonNull Item item);
}
