package ar.test.hernan.testmeli.ui.detail;

import android.support.v7.widget.AppCompatImageView;

import ar.test.hernan.testmeli.app.BaseContracts;
import ar.test.hernan.testmeli.ui.detail.presenter.DetailPresenter;
import ar.test.hernan.testmeli.ui.detail.view.DetailFragment;

/**
 * Contrato con el comportamiento esperado entre {@link DetailFragment} y {@link DetailPresenter}
 * */
public interface DetailContract {
    interface View extends BaseContracts.View<DetailPresenter> {
        AppCompatImageView getDetailImageView();
        void setTitle(String title);
        void setPrice(String price);
        void setCategory(String category);
        void setAvailableQuantity(String quantity);
    }

    abstract class Presenter extends BaseContracts.Presenter<DetailFragment> {

    }
}
