package ar.test.hernan.testmeli.data.source.local;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import ar.test.hernan.testmeli.data.models.Item;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.notNullValue;

@RunWith(AndroidJUnit4.class)
public class ItemsDaoTest {
    private ItemsDataBase mDatabase;
    private static final Item item1 = new Item("ID_123","Titulo", 30.0f, 1, "url", "category");
    private static final Item item2 = new Item("ID_124","Titulo2", 30.0f, 1, "url", "category");
    private static final Item item3 = new Item("ID_125","Titulo3", 30.0f, 1, "url", "category");
    @Before
    public void initDb() {
        mDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                ItemsDataBase.class).build();
    }

    @After
    public void closeDb() {
        mDatabase.close();
    }

    @Test
    public void insertItem(){
        mDatabase.itemDao().insertItem(item1);
        Item loaded = mDatabase.itemDao().getItem(item1.getId());
        assertItems(loaded, "ID_123","Titulo", 30.0f, 1, "url", "category");
    }

    @Test
    public void insertDuplicate(){
        mDatabase.itemDao().insertItem(item1);
        mDatabase.itemDao().insertItem(item1);
        mDatabase.itemDao().insertItem(item1);
        mDatabase.itemDao().insertItem(item1);
        List<Item> items = mDatabase.itemDao().getItems("titulo");
        getItems(items, 1);
    }

    @Test
    public void getItems(){
        mDatabase.itemDao().insertItem(item1);
        mDatabase.itemDao().insertItem(item2);
        mDatabase.itemDao().insertItem(item3);
        List<Item> items = mDatabase.itemDao().getItems("titulo");
        getItems(items, 3);
    }

    private void getItems(List<Item> items, int size){
        assertThat(items, notNullValue());
        assertThat(items.size(), is(size));
    }

    private void assertItems(Item item, String id, String title, float price, int quantity, String thum, String category) {
        assertThat(item, notNullValue());
        assertThat(item.getId(), is(id));
        assertThat(item.getTitle(), is(title));
        assertThat(item.getPrice(), is(price));
        assertThat(item.getAvailableQuantity(), is(quantity));
        assertThat(item.getThumbnail(), is(thum));
        assertThat(item.getCategory(), is(category));
    }
}
