package ar.test.hernan.testmeli.data.source.local;

import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.List;
import java.util.concurrent.Executor;

import ar.test.hernan.testmeli.data.models.Item;
import ar.test.hernan.testmeli.data.source.ItemsDataSource;
import ar.test.hernan.testmeli.util.executorutils.AppExecutors;
import io.reactivex.disposables.Disposable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class ItemsLocalDataSourceTest {
    private ItemsLocalDataSource mLocalDataSource;
    private ItemsDataBase mDatabase;

    private static Executor instant = new Executor() {
        @Override
        public void execute(@NonNull Runnable command) {
            command.run();
        }
    };

    private static final Item item1 = new Item("ID_123","Titulo", 30.0f, 1, "url", "category");
    private static final Item item2 = new Item("ID_124","Titulo2", 30.0f, 1, "url", "category");
    private static final Item item3 = new Item("ID_125","Titulo3", 30.0f, 1, "url", "category");

    @Before
    public void setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        mDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                ItemsDataBase.class)
                .build();
        ItemsDao dao = mDatabase.itemDao();

        // Make sure that we're not keeping a reference to the wrong instance.
        mLocalDataSource = new ItemsLocalDataSource(new AppExecutors(instant,instant), dao);
    }

    @After
    public void cleanUp() {
        mDatabase.close();
    }

    @Test
    public void testPreConditions() {
        assertNotNull(mLocalDataSource);
    }


    @Test
    public void getItems(){
        mLocalDataSource.insertItem(item1);
        mLocalDataSource.insertItem(item2);
        mLocalDataSource.insertItem(item3);

        mLocalDataSource.getItems("titulo", 3, new ItemsDataSource.LoadItemsCallback() {
            @Override
            public void onDisposableAcquired(Disposable disposable) {

            }

            @Override
            public void onItemsLoaded(List<Item> items) {
                assertNotNull(items);
                assertEquals(3, items.size());
            }

            @Override
            public void onDataNotAvailable() {
                fail();
            }
        });
    }
}
