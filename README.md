# Test MeLi

##Introducción
El apk de prueba se encuentra en la carpeta raíz, es el archivo llamado 'app-test-meli.apk'.
Intente no utilizar ningún plugin para facilitar el montaje del proyecto, así que debería bastar solamente con descargar 
y sincronizar las librerías.

Si bien el ejercicio podía resolverse de una forma mas simple ya que era un proyecto pequeño, decidí montar esta arquitectura
para mostrar como utilizaría ciertas herramientas y cual seria la solución que plantearia en casos de mayor magnitud. 

La explicación de por que decidí crear cada clase esta explicada como encabezado en cada uno de los archivos correspondientes.

##Herramientas
    - Dagger 2 para inyectar dependencias
    
    - Junit y Mockito para testeos locales
     
    - Gson para facilitar la comunicación con la API
    
    - Picasso para Cache de imágenes

##Uso 
Es posible hacer consultas de productos online, y luego, hacer una consulta offline sobre productos buscados previamente. 

